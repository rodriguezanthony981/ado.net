﻿using ADO.NET.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

using CapaDatos.Repository;
using CapaDatos.Entitys;

namespace ADO.NET.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITipoMedicamento _repositorio;

        public HomeController(ITipoMedicamento repositorio)
        {
            _repositorio = repositorio;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> MostrarTipoMedicamento()
        {
            List<TipoMedicamento> tipoMedicamentos = await _repositorio.ObtenerTiposMedicamentos();
            return View(tipoMedicamentos);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
