﻿using CapaDatos.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Repository
{
    public interface ITipoMedicamento
    {
        Task<List<TipoMedicamento>> ObtenerTiposMedicamentos();
    }
}
