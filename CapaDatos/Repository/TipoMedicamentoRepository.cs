﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos.Configuration;
using CapaDatos.Entitys;
using Microsoft.Extensions.Options;
using System.Data;
using System.Data.SqlClient;

namespace CapaDatos.Repository
{
    public class TipoMedicamentoRepository : ITipoMedicamento
    {
        private readonly Configuracion _cnx;

        public TipoMedicamentoRepository(IOptions<Configuracion> cnx)
        {
            _cnx = cnx.Value;
        }
        public async Task<List<TipoMedicamento>> ObtenerTiposMedicamentos()
        {
            List<TipoMedicamento> ListaMedicamento = new List<TipoMedicamento>();

            using(var conexion = new SqlConnection(_cnx.DBContext))
            {
                conexion.Open();
                SqlCommand cmd = new SqlCommand("SP_GetTipoMedicamento", conexion);
                cmd.CommandType = CommandType.StoredProcedure;

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        ListaMedicamento.Add(new TipoMedicamento()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Nombre = Convert.ToString(reader["Nombre"]),
                            Descripcion = Convert.ToString(reader["Descripcion"]),
                            Habilitado = Convert.ToInt16(reader["Habilitado"]),
                        });
                    }
                }
            }
            return ListaMedicamento;
        }
    }
}
